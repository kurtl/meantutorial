/**
 * Created by kurtl on 5/13/2015.
 */

exports.sayHelloInEnglish = function() {
    return "Hello";
};


exports.sayHelloInSpanish = function() {
    return "Hola";
};

exports.someObject = {foo: 'bar'};


console.log('exports : ' + exports);
console.log('module.exports : ' +  module.exports);

console.log(exports.sayHelloInEnglish());

//module.exports = "Bonjour";

//console.log('exports : ' + exports);
//console.log('module.exports : ' +  module.exports);