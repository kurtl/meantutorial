var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
    title: String,
    link: String,
    upvotes: {type: Number, default: 0},
    upvoteUsers: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    comments: [{type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}],
    author: String
});


PostSchema.methods.upvote = function (currentUser, cb) {
    if (this.upvoteUsers.indexOf(currentUser._id) > -1) {
        throw new Error('user: ' + currentUser.username + ' already upvoted');
    }
    this.upvotes += 1;
    this.upvoteUsers.push(currentUser);
    this.save(cb);
};


PostSchema.methods.downvote = function (cb) {
    if (this.upvotes <= 0) {
        return; // TODO throw instead
    }
    this.upvotes -= 1;
    this.save(cb);
};

mongoose.model('Post', PostSchema);