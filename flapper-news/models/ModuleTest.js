/**
 * Created by kurtl on 5/13/2015.
 */

// simple file to understand require, exports, and module.exports
// http://www.sitepoint.com/understanding-module-exports-exports-node-js/


var moduleTest2 = require('./ModuleTest2');
console.log('ModuleTest2 : ' + moduleTest2);

console.log(moduleTest2.sayHelloInEnglish());
console.log(moduleTest2.someObject);

var moduleTest3 = require('./ModuleTest3');
console.log('ModuleTest3 : ' + moduleTest3);

console.log(moduleTest3.sayHelloInEnglish());
console.log(moduleTest3.someObject);

