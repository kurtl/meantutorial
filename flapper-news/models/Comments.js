var mongoose = require('mongoose');

var CommentSchema = new mongoose.Schema({
  body: String,
  author: String,
  upvotes: {type: Number, default: 0},
  post: { type: mongoose.Schema.Types.ObjectId, ref: 'Post' }
});

CommentSchema.methods.upvote = function(cb) {
  this.upvotes += 1;
  this.save(cb);
};

// A constructor-ish static method
CommentSchema.statics.createNew =  function (body, author, post) {
  if (!body) {
    throw new Error('body is required for a comment');
  }
  if (!author) {
    author = 'Anonymous';
  }
  var Comment = mongoose.model('Comment');
  var comment = new Comment({
    body: body,
    author: author,
    post: post
  });
  return comment;
};

var Comment = mongoose.model('Comment', CommentSchema);

