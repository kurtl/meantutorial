var express = require('express');
var router = express.Router();
var passport = require('passport');
var jwt = require('express-jwt');

var async = require('async');


require('../util/Date');
var mongoose = require('mongoose');
var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');
var User = mongoose.model('User');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

// log time of each reqeust
//router.use(function timeLog(req, res, next) {
//    var newDate = new Date();
//    console.log('Timestamp: ', newDate.today() + " @ " + newDate.timeNow());
//    next();
//});

// our authentication function. used in posts and puts
var auth = jwt({secret: 'SECRET', userProperty: 'payload'});


var currentUser;

// Attempt to fetch the user after authentication.
// auth calling next (invokes next handler) doesn't allow for this approach
// We need a separate handler (see gtUser)
var auth2 = function(req, res, next) {
    //var jwtResult = jwt({secret: 'SECRET', userProperty: 'payload'});
    var jwtResult = auth(req, res, next);

    if (req.payload.username) {
        User.findOne({username: req.payload.username}, function(err, theUser) {
                if (err) {
                    console.log('can\'t find logged in user' + req.payload.username);
                }
                else {
                    currentUser = theUser;
                }
            }
        );
    }

    return jwtResult;
}

// handler which fetches the User from Mongo after authorization
var getUser = function(req, res, next) {
    if (req.payload.username) {
        User.findOne({username: req.payload.username}, function(err, theUser) {
                if (err) {
                    console.log('can\'t find logged in user' + req.payload.username);
                    next(err);
                }
                else {
                    currentUser = theUser;
                    next();
                }
            }
        );
    }
}


// there isn't a consistent url pattern that can be used, requireing a user is more by method
//router.all('/posts', auth, getUser);


/* - Posts REST interface - */

// fetch the post by ID for all REST operations that have a post argument
// adds that post to the req object for subsequent processing by the next route thingy
router.param('post', function(req, res, next, id) {
    var query = Post.findById(id);

    query.exec(function (err, post){
        if (err) { return next(err); }
        if (!post) { return next(new Error('can\'t find post')); }

        req.post = post;
        return next();
    });
});

// get all posts
router.get('/posts', function (req, res, next) {
    Post.find(function(err, posts){
        if (err) {
            return next(err);
        }
        res.json(posts);
    });
});

// get a single post
router.get('/posts/:post', function(req, res) {
    req.post.populate('comments', function(err, post) {
        if (err) { return next(err); }

        res.json(post);
    });
});

// create a post
router.post('/posts', auth, getUser, function(req, res, next) {
    var post = new Post(req.body);
    post.author = currentUser.username;
    console.log('currentUser: ' + currentUser._id);
    post.save(function(err, post){
        if(err){ return next(err); }

        res.json(post);
    });
});

// upvote a post
router.put('/posts/:post/upvote', auth, getUser, function(req, res, next) {
    req.post.upvote(currentUser, function(err, post){
        if (err) { return next(err); }

        res.json(post);
    });
});

// downvote a post
router.put('/posts/:post/downvote', auth, function(req, res, next) {
    req.post.downvote(function(err, post){
        if (err) { return next(err); }

        res.json(post);
    });
});


/***** Comments ****/
// fetch the comment by ID for all REST operations that have a comment argument
router.param('comment', function(req, res, next, id) {
    var query = Comment.findById(id);

    query.exec(function (err, comment){
        if (err) { return next(err); }
        if (!comment) { return next(new Error('can\'t find comment')); }

        req.comment = comment;
        return next();
    });
});


// add a comment
router.post('/posts/:post/comments', auth,  function(req, res, next) {
//    var comment = new Comment(req.body);
//    comment.post = req.post;
    var comment = Comment.createNew(req.body.body, req.payload.username, req.post);
    comment.save(function(err, comment){
        if(err){ return next(err); }

        req.post.comments.push(comment);
        req.post.save(function(err, post) {
            if(err){ return next(err); }

            res.json(comment);
        });
    });
});


// upvote a comment
router.put('/posts/:post/comments/:comment/upvote', auth, function(req, res, next) {
    req.comment.upvote(function(err, comment){
        if (err) { return next(err); }

        res.json(comment);
    });
});


/* User stuff */

router.post('/register', function(req, res, next){
    if(!req.body.username || !req.body.password){
        return res.status(400).json({message: 'Please fill out all fields'});
    }

    var user = new User();

    user.username = req.body.username;

    user.setPassword(req.body.password)

    user.save(function (err){
        if(err){ return next(err); }

        return res.json({token: user.generateJWT()})
    });
});

router.post('/login', function(req, res, next){
    if(!req.body.username || !req.body.password){
        return res.status(400).json({message: 'Please fill out all fields'});
    }

    passport.authenticate('local', function(err, user, info){
        if(err){ return next(err); }

        if(user){
            return res.json({token: user.generateJWT()});
        } else {
            return res.status(401).json(info);
        }
    })(req, res, next);
});

module.exports = router;
