//http://angular-ui.github.io/ui-router/site/#/api/ui.router
var app = angular.module('flapperNews', ['ui.router'])

/* Configuration */
app.config([
    '$stateProvider',
    '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: '/home.html',
                controller: 'MainCtrl',
                resolve: { // load all the posts when the home page is requested
                    postPromise: ['posts', function(posts){
                        return posts.getAll();
                    }]
                }
            })

            .state('posts', {
                url: '/posts/{id}',
                templateUrl: '/posts.html',
                controller: 'PostsCtrl',
                resolve: { // load the post in context
                    post: ['$stateParams', 'posts', function ($stateParams, posts) {
                        return posts.get($stateParams.id);
                    }]
                }
            })

            .state('login', {
                url: '/login',
                templateUrl: '/login.html',
                controller: 'AuthCtrl',
                onEnter: ['$state', 'auth', function($state, auth){
                    if(auth.isLoggedIn()){
                        $state.go('home');
                    }
                }]
            })

            .state('register', {
                url: '/register',
                templateUrl: '/register.html',
                controller: 'AuthCtrl',
                onEnter: ['$state', 'auth', function($state, auth){
                    if(auth.isLoggedIn()){
                        $state.go('home');
                    }
                }]
            });

        $urlRouterProvider.otherwise('home');
    }])

/* Authentication service */
.factory('auth', ['$http', '$window', function($http, $window){
    var auth = {};

    auth.saveToken = function (token){
        $window.localStorage['flapper-news-token'] = token;
    };

    auth.getToken = function (){
        return $window.localStorage['flapper-news-token'];
    }

    auth.isLoggedIn = function(){
        var token = auth.getToken();

        if(token){
            var payload = JSON.parse($window.atob(token.split('.')[1]));

            return payload.exp > Date.now() / 1000;
        } else {
            return false;
        }
    };

    auth.currentUser = function(){
        if(auth.isLoggedIn()){
            var token = auth.getToken();
            var payload = JSON.parse($window.atob(token.split('.')[1]));

            return payload.username;
        }
    };

    auth.register = function(user){
        return $http.post('/register', user).success(function(data){
            auth.saveToken(data.token);
        });
    };

    auth.logIn = function(user){
            return $http.post('/login', user).success(function(data){
                auth.saveToken(data.token);
            });
        };

    auth.logOut = function(){
        $window.localStorage.removeItem('flapper-news-token');
    };

    return auth;
}])

/* posts service: this should be renamed to somethings besides posts */
.factory('posts', ['$http', 'auth', function($http, auth){
    var o = {
        posts: []
    };

    function authHeader() {
        return {
            headers: {Authorization: 'Bearer ' + auth.getToken()}
        };
    }

    o.getAll = function() {
        return $http.get('/posts').success(function(data){
            angular.copy(data, o.posts);
        });
    };

    o.get = function(id) {
        return $http.get('/posts/' + id).then(function(res){
            return res.data;
        });
    };



        o.create = function(post) {
        return $http.post('/posts', post, authHeader()).success(function(data){
            o.posts.push(data);
        });
    };

    o.upvote = function(post) {
        return $http.put('/posts/' + post._id + '/upvote', null, authHeader())
            .success(function(data){
                post.upvotes += 1;
            }).error(function(data, status, headers, config) {
                var obj = JSON.parse( data );
                    alert('error message: ' + obj.message);
            })
    };

    o.downvote = function(post) {
        if (post.upvotes<=0) {
            throw new Error('can\'t downvote a post with with 0 upvotes');
        }
        return $http.put('/posts/' + post._id + '/downvote', null, authHeader())
            .success(function(data){
                post.upvotes -= 1;
            });
    };

    o.addComment = function(id, comment) {
        return $http.post('/posts/' + id + '/comments', comment, authHeader());
    };

    o.upvoteComment = function(comment) {
        //comment.post is actually just its ID
        return $http.put('/posts/' + comment.post + '/comments/' + comment._id + '/upvote', null, authHeader())
            .success(function(data){
                comment.upvotes += 1;
            });
    };

    return o;
}]);


/* Main Controller */
app.controller('MainCtrl', [
    '$scope',
    'posts',
    'auth',
    function($scope, posts, auth){
        $scope.posts = posts.posts;
        $scope.isLoggedIn = auth.isLoggedIn;
        $scope.addingPost = false;

        $scope.addPost = function(){
            if(!$scope.title || $scope.title === '') { return; }
            posts.create({
                title: $scope.title,
                link: $scope.link
            });
            $scope.title = '';
            $scope.link = '';
        };



        $scope.incrementUpvotes = function(post) {
            posts.upvote(post);
        };

        $scope.decrementUpvotes = function(post) {
            posts.downvote(post);
        };


        $scope.toggleAddPost = function() {
            $scope.addingPost = !$scope.addingPost;
        };


    }]);


/* Posts Controller */
app.controller('PostsCtrl', [
    '$scope',
    'posts',
    'post',
    'auth',
    function($scope, posts, post, auth){
        $scope.post = post;
        $scope.isLoggedIn = auth.isLoggedIn;

        $scope.addComment = function(){
//            if($scope.body === '') { return; }
//            if(!$scope.body) {
//                throw new Error("Can not add a comment with an empty body");
//            }
            posts.addComment(post._id, {
                body: $scope.body,
                author: 'user'
            }).success(function(comment) {
                $scope.post.comments.push(comment);
            });
            $scope.body = '';
        };

        $scope.incrementUpvotes = function(comment) {
            posts.upvoteComment(comment);
        }

    }]);

app.controller('AuthCtrl', [
    '$scope',
    '$state',
    'auth',
    function($scope, $state, auth){
        $scope.user = {};

        $scope.register = function(){
            auth.register($scope.user).error(function(error){
                $scope.error = error;
            }).then(function(){
                $state.go('home');
            });
        };

        $scope.logIn = function(){
            auth.logIn($scope.user).error(function(error){
                $scope.error = error;
            }).then(function(){
                $state.go('home');
            });
        };
    }]);

app.controller('NavCtrl', [
    '$scope',
    'auth',
    function($scope, auth){
        $scope.isLoggedIn = auth.isLoggedIn;
        $scope.currentUser = auth.currentUser;
        $scope.logOut = auth.logOut;
    }]);